/**
 * @file main.c
 * @author Byron Quezada
 * @date 15/05/2021
 * @brief Basic example for a Linux system call.
 *
 * @see https://github.com/docker-library/hello-world
 */

#include <sys/syscall.h>
#include <unistd.h>

const char mensaje[] =
	"\n"
	"Este mensaje muestra que el programa se ejecutó correctamente\n"
	"\n";

int main() {
	syscall(SYS_write, STDOUT_FILENO, mensaje, sizeof(mensaje) - 1);

	return 0;
}


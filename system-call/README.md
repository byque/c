# Compilación
`make`

## Docker
```
docker build -f Dockerfile.build -t llamada-sistema:construir .
docker run --rm llamada-sistema:construir \
  sh -c 'find \( -name 'llamada' \) -print0 | \
  xargs -0 tar --create' | tar --extract --verbose
docker build -t llamada-sistema:actual .
```

# Ejecución
`./llamada`

## Docker
`docker run --rm llamada-sistema:actual`
